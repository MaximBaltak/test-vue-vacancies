import axios from 'axios'

export default {
  namespaced: true,
  state: {
    vacancies: [],
    filterVacancies: [],
    filters: {
      region: null,
      city: null,
      company: null
    },
    regions: [],
    cites: [],
    company: []
  },
  actions: {
    async getVacancies ({ commit }) {
      try {
        const { data } = await axios.get('https://gsr-rabota.ru/api/v2/GetAllVacancies')
        commit('initVacancies', data)
        commit('initRegions', data)
        commit('initCites', data)
        commit('initCompany', data)
      } catch (e) {
        console.log(e)
      }
    }
  },
  mutations: {
    initVacancies (state, vacancies) {
      state.vacancies = [...vacancies]
      state.filterVacancies = [...vacancies]
    },
    initRegions (state, vacancies) {
      vacancies.forEach(vacancy => {
        for (let i = 0; i < state.regions.length; i++) {
          if (state.regions[i]?.id === vacancy.region_id) {
            return
          }
        }
        if (vacancy.region_id) {
          const region = {
            id: vacancy.region_id,
            name: vacancy.regionname
          }
          state.regions.push(region)
        }
      })
    },
    initCites (state, vacancies) {
      vacancies.forEach(vacancy => {
        for (let i = 0; i < state.cites.length; i++) {
          if (state.cites[i]?.id === vacancy.placeid) {
            return
          }
        }
        if (vacancy.placeid) {
          const city = {
            id: vacancy.placeid,
            name: vacancy.placetitle
          }
          state.cites.push(city)
        }
      })
    },
    initCompany (state, vacancies) {
      vacancies.forEach(vacancy => {
        for (let i = 0; i < state.company.length; i++) {
          if (state.company[i]?.id === vacancy.clientid) {
            return
          }
        }
        if (vacancy.clientid) {
          const company = {
            id: vacancy.clientid,
            name: vacancy.clientname
          }
          state.company.push(company)
        }
      })
    },
    updateFilterRegion (state, region) {
      state.filters.region = { ...region }
    },
    updateFilterCity (state, city) {
      state.filters.city = { ...city }
    },
    updateFilterCompany (state, company) {
      state.filters.company = { ...company }
    },
    clearFilter (state) {
      state.filters.company = null
      state.filters.city = null
      state.filters.region = null
      state.filterVacancies = [...state.vacancies]
    },
    filterVacancies (state) {
      state.filterVacancies = [...state.vacancies]
      const region = Boolean(state.filters.region)
      const company = Boolean(state.filters.company)
      const city = Boolean(state.filters.city)
      if (region) {
        const result = state.filterVacancies.filter(vacancy => vacancy.region_id === state.filters.region.id)
        state.filterVacancies = [...result]
      }
      if (company) {
        const result = state.filterVacancies.filter(vacancy => vacancy.clientid === state.filters.company.id)
        state.filterVacancies = [...result]
      }
      if (city) {
        const result = state.filterVacancies.filter(vacancy => vacancy.placeid === state.filters.city.id)
        state.filterVacancies = [...result]
      }
    }
  }
}
