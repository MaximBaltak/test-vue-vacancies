import Vue from 'vue'
import Vuex from 'vuex'
import vacancies from '@/store/vacancies'

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  state: {
    isOpenModalResponse: false,
    isLoading: false
  },
  getters: {},
  actions: {},
  mutations: {
    toggleModalResponse (state, value) {
      state.isOpenModalResponse = value
    },
    toggleLoading (state, value) {
      state.isLoading = value
    }
  },
  modules: {
    vacancies
  }
})
