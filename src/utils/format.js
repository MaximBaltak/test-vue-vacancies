export const stringCountVacancies = (number) => {
  const lastSymbol = number.toString().at(-1)
  const prevSymbol = number.toString().at(-2)
  if (prevSymbol === '1') return `${number} вакансий`
  let str = ''
  switch (lastSymbol) {
    case '1':
      str = 'вакансия'
      break
    case '2':
    case '3':
    case '4':
      str = 'вакансии'
      break
    default:
      str = 'вакансий'
      break
  }
  return `${number} ${str}`
}
