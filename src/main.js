import Vue from 'vue'
import App from './App.vue'
import store from './store'
import { Multiselect } from 'vue-multiselect'
Vue.config.productionTip = false
Vue.component('MultiSelect', Multiselect)
new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
